/**
 * Flamenco manager
 * Render Farm manager API
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import WorkerTag from './WorkerTag';

/**
 * The EventWorkerTagUpdate model module.
 * @module model/EventWorkerTagUpdate
 * @version 0.0.0
 */
class EventWorkerTagUpdate {
    /**
     * Constructs a new <code>EventWorkerTagUpdate</code>.
     * Worker Tag, sent over SocketIO/MQTT when it changes. 
     * @alias module:model/EventWorkerTagUpdate
     * @param tag {module:model/WorkerTag} 
     */
    constructor(tag) { 
        
        EventWorkerTagUpdate.initialize(this, tag);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, tag) { 
        obj['tag'] = tag;
    }

    /**
     * Constructs a <code>EventWorkerTagUpdate</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EventWorkerTagUpdate} obj Optional instance to populate.
     * @return {module:model/EventWorkerTagUpdate} The populated <code>EventWorkerTagUpdate</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EventWorkerTagUpdate();

            if (data.hasOwnProperty('tag')) {
                obj['tag'] = WorkerTag.constructFromObject(data['tag']);
            }
            if (data.hasOwnProperty('was_deleted')) {
                obj['was_deleted'] = ApiClient.convertToType(data['was_deleted'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/WorkerTag} tag
 */
EventWorkerTagUpdate.prototype['tag'] = undefined;

/**
 * When a tag was just deleted, this is set to `true`.
 * @member {Boolean} was_deleted
 */
EventWorkerTagUpdate.prototype['was_deleted'] = undefined;






export default EventWorkerTagUpdate;

